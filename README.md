# Addon Boilerplate

Isleward addon userscript boilerplate.

* Webpack: bundle dependencies and styles, minimize the output
* Webpack-Dev-Server: proxy userscript for hot reloading

## Usage

* Clone and run `yarn`
* Run `yarn dev` to create a "proxy" userscript for TamperMonkey [(more info)](https://github.com/momocow/webpack-userscript#hot-development)
* Userscripts are built to `build/` locally
* Userscripts built in CI (not fully set up by default) are in `dist/` and committed

## Extending

* If you use GM functions (`@grant` other than `none`), you might need to use `unsafeWindow`?

### GitLab CI

You can build and commit the output userscript to `dist/` on every commit to master using the almost-ready `.gitlab-ci-example.yml`.
Rename it to `.gitlab-ci.yml` and fill in the Git email and username commands.
You will also need to add the `SSH_PRIVATE_KEY` to GitLab CI environment variables in project settings.

### ESLint

* `yarn add -D eslint eslint-plugin-react eslint-plugin-react-hooks`
* Add `"lint": "eslint ."` to scripts
* Create `.eslintrc.json` (remove `ecmaFeatures.jsx` if you don't use it, and tweak whatever else):

```json
{
	"env": {
		"browser": true,
		"es2020": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:react-hooks/recommended"
	],
	"parserOptions": {
		"ecmaVersion": 12,
		"sourceType": "module",
		"ecmaFeatures": {
			"jsx": true
		}
	},
	"globals": {
		"unsafeWindow": "writable",
		"$": "readonly"
	},
	"rules": {
		"react/prop-types": "off"
	}
}
```

### React/Babel

* `yarn add -D @babel/core babel-loader`
* Create `.babelrc` with `@babel/preset-env` and `@babel/preset-react`
* Add this to `module.rules` in the Webpack config:

```js
{
	test: /\.jsx?$/,
	loader: 'babel-loader',
	exclude: /node_modules/,
},
```
